package mapper;

import entity.Category;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by ciuscheung on 17/4/1.
 */
public class CategoryMapperTest {

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void init() throws IOException {
        String resource = "mybatis/SqlMapConfig.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }


    @Test
    public void findCategoryList() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession();

        CategoryMapper categoryMapper = sqlSession.getMapper(CategoryMapper.class);

        List<Category> categories = categoryMapper.findCategoryList();

        sqlSession.close();

        System.out.println(categories);
    }

}