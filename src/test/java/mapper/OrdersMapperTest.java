package mapper;

import entity.Orders;
import entity.OrdersUserVo;
import entity.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by ciuscheung on 17/3/31.
 */
public class OrdersMapperTest {

    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void init() throws IOException {
        String resource = "mybatis/SqlMapConfig.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }

    @Test
    public void findOrdersUser() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        OrdersMapper ordersMapper = sqlSession.getMapper(OrdersMapper.class);

        List<OrdersUserVo> ordersUserVos = ordersMapper.findOrdersUser();
        sqlSession.close();
        System.out.println(ordersUserVos);
    }

    @Test
    public void findOrdersUserMap() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        OrdersMapper ordersMapper = sqlSession.getMapper(OrdersMapper.class);

        List<Orders> ordersUsers = ordersMapper.findOrdersUserMap();
        sqlSession.close();
        System.out.println(ordersUsers);
    }

    @Test
    public void findOrdersAndDetailMap() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        OrdersMapper ordersMapper = sqlSession.getMapper(OrdersMapper.class);

        List<Orders> ordersUsers = ordersMapper.findOrdersAndDetailMap();
        sqlSession.close();
        System.out.println(ordersUsers);
    }

    @Test
    public void findUserProductResultMap() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        OrdersMapper ordersMapper = sqlSession.getMapper(OrdersMapper.class);

        List<User> ordersUsers = ordersMapper.findUserProductResultMap();
        sqlSession.close();
        System.out.println(ordersUsers);
    }

    @Test
    public void findOrdersUserLazyLoading() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        OrdersMapper ordersMapper = sqlSession.getMapper(OrdersMapper.class);

        List<Orders> list = ordersMapper.findOrdersUserLazyLoading();
//        遍历订单列表
        for (Orders order: list) {
//            执行getUer()去查询用户信息,实现按需要加载
            User user = order.getUser();
            System.out.println(user);
        }
        sqlSession.close();

    }

}