package mapper;

import entity.User;
import entity.UserCustomer;
import entity.UserQueryVo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ciuscheung on 17/3/27.
 */
public class UserMapperTest {
    private SqlSessionFactory sqlSessionFactory;

    @Before
    public void init() throws IOException {
        String resource = "mybatis/SqlMapConfig.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }
    @Test
    public void findUserById() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //创建UserMapper对象,mybatis自动生成mapper代理对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        User user = userMapper.findUserById(1);
        sqlSession.close();
        System.out.println(user);
    }

    @Test
    public void findUserByName() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //创建UserMapper对象,mybatis自动生成mapper代理对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        List<User> users = userMapper.findUserByName("kk");
        sqlSession.close();
        System.out.println(users);
    }

    @Test
    public void findUserList() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //创建UserMapper对象,mybatis自动生成mapper代理对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        UserQueryVo userQueryVo = new UserQueryVo();
        UserCustomer userCustomer = new UserCustomer();
        //由于这里使用了动态Sql,如果不设置某个值,条件不会拼接到sql中
//        userCustomer.setAge(34);
//        userCustomer.setName("张三丰");
        //传入多个id
        List<Integer> ids = new ArrayList<Integer>();
        ids.add(1);
        ids.add(2);
        ids.add(5);
        //将ids通过userQueryVo传入statement中
        userQueryVo.setIds(ids);
        userQueryVo.setUserCustomer(userCustomer);
        List<UserCustomer> users = userMapper.findUserList(userQueryVo);
        sqlSession.close();
        System.out.println(users);
    }

    @Test
    public void findUserCount() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //创建UserMapper对象,mybatis自动生成mapper代理对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        UserQueryVo userQueryVo = new UserQueryVo();
        UserCustomer userCustomer = new UserCustomer();
        //由于这里使用了动态Sql,如果不设置某个值,条件不会拼接到sql中
        userCustomer.setAge(23);
        userQueryVo.setUserCustomer(userCustomer);
        int count = userMapper.findUserCount(userQueryVo);
        sqlSession.close();
        System.out.println(count);
    }

    @Test
    public void getResultMapUser() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //创建UserMapper对象,mybatis自动生成mapper代理对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        List<User> users = userMapper.getResultMapUser();
        sqlSession.close();
        System.out.println(users);
    }

//    一级缓存测试
    @Test
    public void testCacheFirst()throws Exception{
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

//        第一次发起请求,查询id为1的用户,查询时会先查看是否有一级缓存,若没有则新建一个缓存
        User user1 = userMapper.findUserById(1);
        System.out.println(user1);

//        如果sqlSession执行了commit操作(执行插入,删除,更新),会清空selSession中的一级缓存,这样做目的是为了让缓存中存储的是最新的信息,避免脏读.
        user1.setAge(3);
        userMapper.updateUser(user1);
        sqlSession.commit();

//        第二次发起请求,查询id为1的用户,查询时会先查看是否有一级缓存,若有则直接从缓存中读取数据,不需要发送sql语句从数据库读取
        User user2 = userMapper.findUserById(1);
        System.out.println(user2);
        sqlSession.close();
    }

//    二级缓存测试
    @Test
    public void testCacheSecond()throws Exception{
        SqlSession sqlSession1 = sqlSessionFactory.openSession();
        SqlSession sqlSession2 = sqlSessionFactory.openSession();
        SqlSession sqlSession3 = sqlSessionFactory.openSession();

//      第一次发起请求,查询id为1的用户
        UserMapper userMapper1 = sqlSession1.getMapper(UserMapper.class);
        User user1 = userMapper1.findUserById(1);
        System.out.println(user1);
        sqlSession1.close();

//      使用sqlSession3执行commit()操作
        UserMapper userMapper3 = sqlSession3.getMapper(UserMapper.class);
        User user3 = userMapper3.findUserById(1);
        user3.setAge(3);
        userMapper3.updateUser(user3);
//      执行提交操作,清空UserMapper下的二级缓存
        sqlSession3.commit();
        sqlSession3.close();

//      第二次发起请求,查询id为1的用户,若有二级缓存存在,就不需要从数据库中读取,直接从缓存中读取
        UserMapper userMapper2 = sqlSession2.getMapper(UserMapper.class);
        User user2 = userMapper2.findUserById(1);
        System.out.println(user2);
        sqlSession2.close();
    }

}