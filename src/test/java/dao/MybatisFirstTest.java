package dao;

import entity.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by ciusc on 2017/3/26.
 */
public class MybatisFirstTest {

	static String resource;
	static InputStream inputStream;
	static SqlSessionFactory sqlSessionFactory;
	static SqlSession sqlSession;

	public static SqlSession getSession() throws IOException {
		//mybatis配置文件
		resource = "mybatis/SqlMapConfig.xml";
		//得到配置文件流
		inputStream = Resources.getResourceAsStream(resource);
		//创建会化工厂
		sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		//通过工厂得到SqlSession
		sqlSession = sqlSessionFactory.openSession();
		return sqlSession;
	}


	@Test
	public void getUserByIdTest() throws IOException {
		//通过SqlSession操作数据库
		//第一个参数是映射文件中statement的id
		//第二个参数是指定和映射文件中所匹配的parameterType类型的参数
		//sqlSession.selectOne结果是与映射文件中的所匹配的resultType类型的对象
		sqlSession = getSession();
		User user = sqlSession.selectOne("user.findUserById",1);
		System.out.println(user);
		sqlSession.close();
	}

	@Test
	public void deleteUser() throws IOException {
		sqlSession = getSession();
		sqlSession.delete("user.deleteUserById",2);
		sqlSession.commit();
		sqlSession.close();
	}

	@Test
	public void findUserByName() throws IOException {
		sqlSession = getSession();
		List<User> users = sqlSession.selectList("user.findUserByName","xm");
		sqlSession.close();
		System.out.println(users);
	}

	@Test
	public void insertUser() throws IOException {
		sqlSession = getSession();
		User user = new User();
		user.setName("张三丰");
		sqlSession.insert("insertUser",user);
		sqlSession.commit();
		System.out.println("刚插入的id为:" + user.getId());
		sqlSession.close();
	}

	@Test
	public void updateUser() throws IOException {
		sqlSession = getSession();
		User user = new User();
		user.setId(9);
		user.setName("gsagasgga");
		sqlSession.update("user.updateUser",user);
		sqlSession.commit();
		sqlSession.close();
	}
}
