package dao;

import entity.User;
import org.apache.ibatis.session.SqlSessionFactory;

/**
 * Created by ciuscheung on 17/3/27.
 */
public interface UserDao {

    public void insertUser(User user);

    public User getUserById(int id);

    public void deleteUser(int id);
}
