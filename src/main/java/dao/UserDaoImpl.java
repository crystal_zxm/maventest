package dao;

import entity.User;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

/**
 * Created by ciuscheung on 17/3/27.
 */
public class UserDaoImpl implements UserDao {

    private SqlSessionFactory sqlSessionFactory;

    /**
     * 这里注入sqlSessionFactory
     * 通过构造方法注入
     * @param sqlSessionFactory
     */
    public UserDaoImpl(SqlSessionFactory sqlSessionFactory){
        this.sqlSessionFactory = sqlSessionFactory;
    }

    @Override
    public void insertUser(User user) {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        sqlSession.insert("user.insertUser",user);
        sqlSession.commit();
        sqlSession.close();
    }

    @Override
    public User getUserById(int id) {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        User user = sqlSession.selectOne("user.findUserById",id);
        sqlSession.close();
        return user;
    }

    @Override
    public void deleteUser(int id) {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        sqlSession.insert("user.deleteUserById",id);
        sqlSession.commit();
        sqlSession.close();

    }
}
