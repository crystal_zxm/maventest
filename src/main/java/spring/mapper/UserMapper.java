package spring.mapper;

import entity.User;
import entity.UserCustomer;
import entity.UserQueryVo;

import java.util.List;

/**
 * Created by ciuscheung on 17/3/27.
 */
public interface UserMapper {

    public User findUserById(int id) throws Exception;

}

