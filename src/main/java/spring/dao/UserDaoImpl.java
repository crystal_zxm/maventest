package spring.dao;

import entity.User;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;

/**
 * Created by ciuscheung on 17/3/27.
 */
public class UserDaoImpl extends SqlSessionDaoSupport implements UserDao {

    @Override
    public User getUserById(int id) {
//        继承SqlSessionDaoSupport,通过this.getSqlSession()获得sqlSession
        SqlSession sqlSession = this.getSqlSession();
        User user = sqlSession.selectOne("user.findUserById",id);
        return user;
    }
}
