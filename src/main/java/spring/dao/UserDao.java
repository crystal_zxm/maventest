package spring.dao;

import entity.User;

/**
 * Created by ciuscheung on 17/3/27.
 */
public interface UserDao {

    public User getUserById(int id);
}
