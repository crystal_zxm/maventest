package entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ciusc on 2017/3/26.
 */
public class User implements Serializable{
	private Integer id;//id
	private String name;//姓名
	private int age;//年龄
	private int sex;//性别
	private List<Orders> ordersList;

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Orders> getOrdersList() {
		return ordersList;
	}

	public void setOrdersList(List<Orders> ordersList) {
		this.ordersList = ordersList;
	}

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", name='" + name + '\'' +
				", age=" + age +
				", sex=" + sex +
				'}';
	}
}
