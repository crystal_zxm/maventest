package entity;

/**
 * Created by ciuscheung on 17/3/31.
 */
public class OrdersUserVo extends Orders{
    private int age;
    private int sex;
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "OrdersUserVo{" +
                "age=" + age +
                ", sex=" + sex +
                ", userName='" + userName + '\'' +
                '}';
    }
}
