package entity;

import java.util.List;

/**
 * user综合查询的POJO,封装综合查询条件
 * Created by ciuscheung on 17/3/28.
 */
public class UserQueryVo {

    //传入多个id
    private List<Integer> ids;

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }

    //封装了User类
    private UserCustomer userCustomer;

    public UserCustomer getUserCustomer() {
        return userCustomer;
    }

    public void setUserCustomer(UserCustomer userCustomer) {
        this.userCustomer = userCustomer;
    }
}
