package entity;

import java.util.List;

/**
 * Created by ciuscheung on 17/4/1.
 */
public class Category {
    private int id;
    private int parentId;
    private String name;
    private List<Category> categoryList1;
    private List<Category> categoryList2;
    private List<Category> categoryList3;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Category> getCategoryList1() {
        return categoryList1;
    }

    public void setCategoryList1(List<Category> categoryList1) {
        this.categoryList1 = categoryList1;
    }

    public List<Category> getCategoryList2() {
        return categoryList2;
    }

    public void setCategoryList2(List<Category> categoryList2) {
        this.categoryList2 = categoryList2;
    }

    public List<Category> getCategoryList3() {
        return categoryList3;
    }

    public void setCategoryList3(List<Category> categoryList3) {
        this.categoryList3 = categoryList3;
    }
}
