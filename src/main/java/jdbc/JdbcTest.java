package jdbc;

import org.junit.Test;

import java.sql.*;

/**
 * Created by ciusc on 2017/3/26.
 */
public class JdbcTest {
	@Test
	public void jdbcTest(){
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String url = "jdbc:mysql://localhost:3306/test";
		String user = "root";
		String password = "123456";
		String sql = "select * from mybatis where name =?";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(url,user,password);
			ps = connection.prepareStatement(sql);
			ps.setString(1,"zxm");
			rs = ps.executeQuery();
			while (rs.next()){
				System.out.println(rs.getString("id") + " " + rs.getString("name"));
			}
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			if(rs!=null){
				try {
					rs.close();
				}catch (SQLException e){
					e.printStackTrace();
				}
			}
			if(ps!=null){
				try {
					ps.close();
				}catch (SQLException e){
					e.printStackTrace();
				}
			}
			if(connection!=null){
				try {
					connection.close();
				}catch (SQLException e){
					e.printStackTrace();
				}
			}
		}
	}

}
