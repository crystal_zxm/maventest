package mapper;

import entity.Category;
import entity.Orders;
import entity.OrdersUserVo;
import entity.User;

import java.util.List;

/**
 * Created by ciuscheung on 17/3/31.
 */
public interface CategoryMapper {
   //查询订单及关联的用户信息用resultType实现
   public List<Category> findCategoryList()throws Exception;

}
