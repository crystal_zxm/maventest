package mapper;

import entity.User;
import entity.UserCustomer;
import entity.UserQueryVo;

import java.util.List;

/**
 * Created by ciuscheung on 17/3/27.
 */
public interface UserMapper {
    //接口中的方法名要与statement中的id名一致
    //接口中的输入参数类型要与statement中的parameterType指定的类型一致
    //接口中的返回值的类型要与statement中的resultType指定的类型一致

    //若返回的是一个对象,则调用selectOne方法查询数据库
    //若返回的是一个集合对象,则调用selectList方法查询数据库

    public User findUserById(int id) throws Exception;
    public List<User> findUserByName(String name)throws Exception;
    public void deleteUserById(int id)throws Exception;
    public void insertUser(User user)throws Exception;
    public void updateUser(User user)throws Exception;

    //根据复杂查询条件综合查寻
    public List<UserCustomer> findUserList(UserQueryVo userQueryVo);

    //在sql中指定类名时自定义一个resultMap与pojo属性值作映射而获得自定义的resultMap
    public List<User> getResultMapUser()throws Exception;

    //获取综合查询user个数
    public int findUserCount(UserQueryVo userQueryVo)throws Exception;
}

