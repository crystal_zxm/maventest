package mapper;

import entity.Orders;
import entity.OrdersUserVo;
import entity.User;

import java.util.List;

/**
 * Created by ciuscheung on 17/3/31.
 */
public interface OrdersMapper {
   //查询订单及关联的用户信息用resultType实现
   public List<OrdersUserVo> findOrdersUser()throws Exception;
   //查询订单及关联的用户信息用resultMap实现
   public List<Orders> findOrdersUserMap()throws Exception;
   //查询订单及订单详细信息,关联的用户信息用resultMap实现
   public List<Orders> findOrdersAndDetailMap()throws Exception;
   //   查询用户和用户所购买的商品信息用resultMap实现
   public List<User> findUserProductResultMap()throws Exception;
   //   查询订单及关联的用户信息,用户信息使用延迟加载
   public List<Orders> findOrdersUserLazyLoading()throws Exception;
}
