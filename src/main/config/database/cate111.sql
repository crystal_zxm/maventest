/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50709
 Source Host           : localhost
 Source Database       : cate

 Target Server Type    : MySQL
 Target Server Version : 50709
 File Encoding         : utf-8

 Date: 04/01/2017 23:20:08 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `cate`
-- ----------------------------
DROP TABLE IF EXISTS `cate`;
CREATE TABLE `cate` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `cate_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `cate` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `cate`
-- ----------------------------
BEGIN;
INSERT INTO `cate` VALUES ('0', '0', null), ('1', '1', '0'), ('2', '11', '1'), ('3', '111', '2'), ('4', '1111', '3'), ('5', '2', '0'), ('6', '22', '5'), ('7', '222', '6'), ('8', '2222', '7'), ('9', '3', '0'), ('10', '33', '9'), ('11', '333', '10'), ('12', '3333', '11');

